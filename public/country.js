function getQueryParams(param){
    let queryString = window.location.search
    let urlParams = new URLSearchParams(queryString)
    console.log(urlParams.get(param))
    return urlParams.get(param)
   
}
let country_lists =[]
const countryname=getQueryParams('country')

let backbutton=document.getElementById('back')
backbutton.addEventListener('click',function(){
    window.history.back();

})

function countrydetails(data1){
    let details=data1.find((each)=>{
        if(each.name.official==countryname){
            return each
        }
    })
    let card=document.createElement('div')
   
    card.style = "width:50% ; margin-top: 5%"
    card.id="cards"

    let img=document.createElement('img')
    img.style = "width:100%"

    img.src=details.flags.png

    card.appendChild(img)
    container.appendChild(card)

    let detailsdiv=document.createElement('div')
    
    detailsdiv.className="ml-20 "
    

    let ul=document.createElement('ul')
    ul.className="mt-10 pl-5"
    ul.name="ul"
    let li=document.createElement('li')

    let country=document.createElement('p')

    country.id ='country'

    let population=document.createElement('p')
   
    let region=document.createElement('p')
    region.id="all-regions"
    let nativename=document.createElement('p')
    nativename.className="mt-10"
    let subregion=document.createElement('p')
    let capital=document.createElement('p')
    
   
    
    country.innerHTML=`<b>${details.name.official}<b>`
    nativename.textContent=`Native Name:${details.name.common}`

    subregion.textContent=`Sub Region:${details.subregion}`
    population.textContent=`population:${details.population}`
    region.textContent=`region: ${details.region}`
    capital.textContent=`capital : ${details.capital}`
    li.appendChild(country)
    li.appendChild(nativename)
    li.appendChild(population)
    li.appendChild(region)
    li.appendChild(subregion)
    li.appendChild(capital)
    
    ul.appendChild(li)
    detailsdiv.appendChild(ul)
    container.appendChild(detailsdiv)

    let div2=document.createElement('div')
    div2.className="ml-20 mt-32"
    let ul2=document.createElement('ul')
    let li2=document.createElement('li')
    let topleveldomain=document.createElement('p')
    let currencies=document.createElement('p')
    let languages=document.createElement('p')

    topleveldomain.textContent=`Top Level Domain:${details.tld}`   
    currencies.textContent=`Currencies:${Object.keys(details.currencies)}`
    languages.textContent=`Languages:${Object.entries(details.languages)}`

    li2.appendChild(topleveldomain)
    li2.appendChild(currencies)
    li2.appendChild(languages)
    ul2.appendChild(li2)
    div2.appendChild(ul2)
    


 
    let buttons=document.getElementById('buttons')
    let maindiv=document.createElement('div')

    maindiv.appendChild(detailsdiv)
    maindiv.appendChild(div2)
    maindiv.style.marginTop='100px'
    maindiv.className="mt-10 flex flex-row ml-20 my-40"
   
   

    let button1=document.createElement('button')
    
    maindiv.appendChild(button1)

    container.appendChild(maindiv)
    console.log(details.borders)
    let buttondiv=document.createElement('div')
    buttondiv.className="flex flex-row"

  
        details.borders.forEach(element => {
            
            let button1=document.createElement('button')
            button1.style.width='70px'
            button1.className="border-2"
            button1.textContent=element
            
           

            button1.addEventListener('click',function(){
                let content=button1.textContent
                console.log(content)
                country_lists.forEach((country)=>{
                    if(country.cca3.toLowerCase()==content.toLowerCase()){

                        window.location.href= `countrypage.html?country=${country.name.official}`;

                    }
                })
               

            })
            buttondiv.appendChild(button1)

            
        });
        let p=document.createElement('p')
        p.textContent="borders"
        p.className=" absolute -ml-10"
        p.style.marginLeft='-10%'
        buttons.appendChild(p)
        buttons.appendChild(buttondiv)
    
  
   

   
}



fetch('https://restcountries.com/v3.1/all').
then((data)=>{
    return data.json()
})
.then((data1)=>{
    console.log(data1)
    country_lists = data1
    countrydetails(data1)
})




